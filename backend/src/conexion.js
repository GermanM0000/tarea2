var mysql = require('mysql');
const host = process.env.HOST || '192.168.1.115';
const db = process.env.DB || 'Ejemplo';
const user = process.env.USER || 'root';
const password = process.env.PASS || 'secret';


var conexion = mysql.createConnection({
    host: host,
    database: db,
    user: user,
    password: password,
});

conexion.connect(function(err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});


module.exports = conexion;